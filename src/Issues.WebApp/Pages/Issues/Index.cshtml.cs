﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
// ReSharper disable UnusedMember.Global

namespace Issues.WebApp.Pages.Issues
{
    public sealed class IndexModel : PageModel
    {
        private readonly IIssueLibraryService _isssues;

        public IndexModel(IIssueLibraryService isssues)
        {
            _isssues = isssues;
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public IssueViewModelCollection Items { get; set; }

        public async Task OnGetAsync()
        {
            Items = await _isssues.AllAsync();
        }
    }
}