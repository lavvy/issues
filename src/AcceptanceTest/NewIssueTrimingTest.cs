﻿using System;
using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class NewIssueTrimingTest : UseWebDriver
    {
        private readonly CreationIssuePage _target;

        public NewIssueTrimingTest()
        {
            _target = Shortcuts.NewIssue();
        }

        [Fact]
        public void TrimTitle()
        {
            var created = _target.Submit(new {Title = "  hello,  world!  ", Description = "D"});

            Assert.Equal("hello,  world!", created.Title.Raw);
        }

        [Fact]
        public void TrimSpecialCharactorsTitle()
        {
            var created = _target.Submit(new {Title = "  hello,  world!　 　", Description = "D"});

            Assert.Equal("hello,  world!", created.Title.Raw);
        }

        [Fact]
        public void TrimDescription()
        {
            var created = _target.Submit(new {Title = "T", Description = " a\n b \n"});

            Assert.Equal(N("a\n b"), created.Description.Raw);
        }

        [Fact]
        public void TrimSpecialCharactorsDescription()
        {
            var created = _target.Submit(new {Title = " T", Description = " a\n b 　\n　"});

            Assert.Equal(N("a\n b"), created.Description.Raw);
        }

        private static string N(string text)
        {
            return text?.Replace("\n", Environment.NewLine);
        }
    }
}