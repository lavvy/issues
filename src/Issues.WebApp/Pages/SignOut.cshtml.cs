﻿using System.Threading.Tasks;
using Issues.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages
{
    public sealed class SignOutModel : PageModel
    {
        private readonly IUserSignInService _userSignInService;

        public SignOutModel(IUserSignInService userSignInService)
        {
            _userSignInService = userSignInService;
        }

        // ReSharper disable once UnusedMember.Global
        public async Task<IActionResult> OnGetAsync()
        {
            await _userSignInService.SignOutAsync();

            return Redirect("/Index");
        }
    }
}