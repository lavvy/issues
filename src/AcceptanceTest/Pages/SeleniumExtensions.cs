﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Extensions;

namespace AcceptanceTest.Pages
{
    public static class SeleniumExtensions
    {
        public static void NewInput(this IWebElement element, string text, IWebDriver driver = null)
        {
            if ((text ?? "").Length > 256 && driver != null)
            {
                element.NewInputSmart(text, driver);
            }
            else
            {
                element.NewInputDirect(text);
            }
        }

        public static bool HasClass(this IWebElement element, string @class)
        {
            return element.GetAttribute("class").Split(" ").Any(c => c == @class);
        }

        private static void NewInputDirect(this IWebElement element, string text)
        {
            element.Clear();
            element.SendKeys(text);
        }

        private static void NewInputSmart(this IWebElement element, string text, IWebDriver driver)
        {
            driver.ExecuteJavaScript("arguments[0].value = arguments[1];", element, text);
        }

        public static IWebElement Find(this ISearchContext context, string cssSelectorToFind)
        {
            return context.FindElement(By.CssSelector(cssSelectorToFind));
        }

        public static IEnumerable<IWebElement> Finds(this ISearchContext context, string cssSelectorToFind)
        {
            return context.FindElements(By.CssSelector(cssSelectorToFind));
        }

        public static void GoToPage(this IWebDriver driver, string path)
        {
            var url = new Uri(Environment.GetEnvironmentVariable("TARGET_URL") ?? "http://localhost:5000");
            if (!string.IsNullOrWhiteSpace(path))
            {
                url = new Uri(url, path);
            }
            driver.Navigate().GoToUrl(url);
        }
    }
}