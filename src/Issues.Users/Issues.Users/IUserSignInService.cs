﻿using System.Threading.Tasks;

namespace Issues.Users
{
    public interface IUserSignInService
    {
        Task<ErrorMessageCollection> SignInAsync(UserName user, Password password);
        Task<UserViewModel> GetCurrentUserAsync();
        Task SignOutAsync();
    }
}