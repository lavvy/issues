﻿using System.Linq;
using Xunit;
using static Issues.Domains.ForTest.IssueFactoryForTest;

namespace Issues
{
    public sealed class IssueViewModelCollectionTest
    {
        [Fact]
        public void CastFromIssueCollection()
        {
            var issues = NewIssues(3);

            var result = (IssueViewModelCollection) issues;

            Assert.Equal(3, result.Length);
            Assert.Equal(new[] {1, 2, 3}, result.Select(i => (int) i.Id).ToArray());
        }

        [Fact]
        public void IsEmpty()
        {
            var result = new IssueViewModelCollection(new IssueViewModel[] { });

            Assert.True(result.IsEmpty);
        }

        [Fact]
        public void IsEmptyNotEmpty()
        {
            var result = new IssueViewModelCollection(new[]
            {
                new IssueViewModel()
            });

            Assert.False(result.IsEmpty);
        }
    }
}