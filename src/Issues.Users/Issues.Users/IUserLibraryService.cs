﻿using System.Threading.Tasks;

namespace Issues.Users
{
    public interface IUserLibraryService
    {
        Task<UserViewModel> FindByUserNameAsync(UserName userName);
    }
}