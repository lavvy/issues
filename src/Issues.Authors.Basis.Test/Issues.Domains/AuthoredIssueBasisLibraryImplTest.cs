﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Issues.Users;
using Issues.Users.Domains;
using Moq;
using Xunit;

namespace Issues.Domains
{
    public class AuthoredIssueBasisLibraryImplTest
    {
        public AuthoredIssueBasisLibraryImplTest()
        {
            _mockBasises = new Mock<IIssueBasisLibrary>();
            _mockAuthors = new Mock<IIssueBasisAutoAuthorLibrary>();

            _target = new AuthoredIssueBasisLibraryImpl(_mockBasises.Object, _mockAuthors.Object);
        }

        private readonly AuthoredIssueBasisLibraryImpl _target;
        private readonly Mock<IIssueBasisAutoAuthorLibrary> _mockAuthors;
        private readonly Mock<IIssueBasisLibrary> _mockBasises;
        private readonly User _testUser = User.Authenticated((UserId) 10, (UserName) "testuser");
        private static readonly IssueId TestIssueId = (IssueId) 15;
        private readonly IssueBasis _testBasis = Basis(TestIssueId);

        private static IssueBasis Basis(IssueId issueId)
        {
            return new IssueBasis(issueId, (OccurrenceDateTime) DateTime.Now);
        }

        private static User Authenticated(IssueId id)
        {
            return User.Authenticated((UserId) (int) id, (UserName) $"user{id}");
        }

        [Fact]
        public async Task AllAsync()
        {
            var ids = new[] {11, 12, 13, 14}
                .Select(id => (IssueId) id)
                .ToArray();
            var basises = ids.Select(Basis).ToCollection();

            _mockBasises.Setup(x => x.AllAsync())
                .Returns(Task.FromResult(basises));
            _mockAuthors.Setup(x => x.FindByIssueIdAsync(It.IsAny<IssueId>()))
                .Returns<IssueId>(id => Task.FromResult(Authenticated(id)));

            var result = await _target.AllAsync();

            // id: [#11, #12, #13, #14]
            Assert.Equal(ids, result.Ids().ToArray());
            // userName: [user11, user12, user13, user14]
            Assert.Equal(ids.Select(Authenticated).UserNames().ToArray(),
                result.Authors().UserNames().ToArray());
        }

        [Fact]
        public async Task FindAsync()
        {
            _mockBasises.Setup(x => x.FindAsync(It.Is<IssueId>(p => p.Equals(TestIssueId))))
                .Returns(Task.FromResult(_testBasis));
            _mockAuthors.Setup(x => x.FindByIssueIdAsync(It.Is<IssueId>(p => p.Equals(TestIssueId))))
                .Returns(Task.FromResult(_testUser));

            var result = await _target.FindAsync(TestIssueId);

            Assert.Equal(_testUser, result.Author);
            Assert.Equal(_testBasis.Id, result.Id);
            Assert.Equal(_testBasis.RegistrationAt, result.RegistrationAt);
        }
    }
}