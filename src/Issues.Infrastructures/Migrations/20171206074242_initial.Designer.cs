﻿// <auto-generated />

// ReSharper disable All

using Issues.Contents.Infrastructures;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.Internal;
using System;

namespace Issues.Infrastructures.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20171206074242_initial")]
    partial class initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.0.1-rtm-125");

            modelBuilder.Entity("Issues.Contents.Infrastructures.IssueBasisRow", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("RegistrationAt")
                        .ValueGeneratedOnAdd();

                    b.HasKey("Id");

                    b.ToTable("Issues");
                });

            modelBuilder.Entity("Issues.Contents.Infrastructures.IssueContentRow", b =>
                {
                    b.Property<int>("IssueId");

                    b.Property<string>("Description")
                        .HasMaxLength(5000);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.HasKey("IssueId");

                    b.ToTable("IssueContents");
                });
#pragma warning restore 612, 618
        }
    }
}
