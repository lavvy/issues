﻿using System.Linq;
using System.Threading.Tasks;
using static Issues.Domains.ForTest.IssueBasisFactoryForTest;
using static Issues.Domains.ForTest.IssueContentFactoryForTest;
using static Issues.Users.ForTest.UserFactoryForTest;

namespace Issues.Domains.ForTest
{
    public static class IssueFactoryForTest
    {
        public static Issue NewIssue(int id, string registrationAt = null, string title = null, string description = null)
        {
            var basis = new AuthoredIssueBasis(NewBasis(id, registrationAt), NewAuthenticated(id));
            return new Issue(basis, NewContent(id, title, description));
        }
        
        public static Task<Issue> NewIssueAsync(int id, string registrationAt = null, string title = null, string description = null)
        {
            return Task.FromResult(NewIssue(id, registrationAt, title, description));
        }

        public static IssueCollection NewIssues(int count)
        {
            return Enumerable
                .Range(1, count)
                .Select(id => NewIssue(id))
                .ToCollection();
        }

        public static Task<IssueCollection> NewIssuesAsync(int count)
        {
            return Task.FromResult(NewIssues(count));
        }
    }
}