﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Identity
{
    internal static class PasswordValidatorEnumerableExtensions
    {
        public static async Task<IdentityResult> ValidateAsync<T>(this IEnumerable<IPasswordValidator<T>> validators,
            UserManager<T> userManager, T user, string password) where T : class
        {
            return await ValidateUtility.ValidateInternalAsync(validators,
                async v => await v.ValidateAsync(userManager, user, password));
        }
    }
}