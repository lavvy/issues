﻿using System;
using Xunit;
using static Issues.Domains.ForTest.IssueFactoryForTest;

namespace Issues
{
    public sealed class IssueViewModelTest
    {
        [Fact]
        public void FromIssue()
        {
            var issue = NewIssue(10, "2018/5/8", "hello", "my name is");

            var result = (IssueViewModel) issue;

            Assert.Equal(10, (int) result.Id);
            Assert.Equal(new DateTime(2018, 5, 8), (DateTime) result.RegistrationAt);
            Assert.Equal("hello", result.Content.Title);
            Assert.Equal("my name is", result.Content.Description);
            Assert.Equal("user10", (string) result.Author.UserName);
        }
    }
}