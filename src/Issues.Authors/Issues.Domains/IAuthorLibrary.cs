﻿using System.Threading.Tasks;
using Issues.Users.Domains;

namespace Issues.Domains
{
    public interface IAuthorLibrary
    {
        Task<User> FindByResourceIdAsync(ResourceId resourceId, ResourceType type);
        Task BindAsync(ResourceId resourceId, ResourceType type, User user);
    }
}