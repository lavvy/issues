﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class CreationIssuePage : ModifilableIssuePage
    {
        public CreationIssuePage(IWebDriver driver) : base(driver)
        {
        }

        public IssuePage Submit(dynamic issue)
        {
            return Post(issue, Find(".issue-edit .submit"));
        }
    }
}