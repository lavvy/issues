﻿using System.Threading.Tasks;
using Issues.Users.Domains;

namespace Issues.Users
{
    public sealed class UserRegistrationServiceImpl : IUserRegistrationService
    {
        private readonly IUserRepository _users;

        public UserRegistrationServiceImpl(IUserRepository users)
        {
            _users = users;
        }

        public async Task<ErrorMessageCollection> RegisterAsync(UserName userName, Password passowrd)
        {
            return await _users.CreateAsync(userName, passowrd);
        }

        public Task<ErrorMessageCollection> ValidatePasswordAsync(Password password)
        {
            return _users.ValidatePasswordAsync(password);
        }

        public Task<ErrorMessageCollection> ValidateUserNameAsync(UserName userName)
        {
            return _users.ValidateUserNameAsync(userName);
        }
    }
}