﻿namespace Issues.Users.Domains
{
    public enum UserType
    {
        Unknown = 0,
        Anonymouse = 1,
        Authenticated = 2
    }
}