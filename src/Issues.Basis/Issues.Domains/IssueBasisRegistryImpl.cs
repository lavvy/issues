﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public sealed class IssueBasisRegistryImpl : IIssueBasisRegistry
    {
        private readonly IIssueBasisRepository _basises;
        private readonly ITimestamp _timestamp;

        public IssueBasisRegistryImpl(IIssueBasisRepository basises, ITimestamp timestamp)
        {
            _basises = basises;
            _timestamp = timestamp;
        }

        public async Task<IssueId> CreateAsync()
        {
            return await _basises.CreateAsync(_timestamp.Now());
        }
    }
}