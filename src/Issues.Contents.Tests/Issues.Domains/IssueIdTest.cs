﻿using Xunit;

namespace Issues.Domains
{
    public sealed class IssueIdTest
    {
        [Fact]
        public void CastToInt()
        {
            var result = (int) new IssueId(15);
            
            Assert.Equal(15, result);
        }

        [Fact]
        public void CastFromInt()
        {
            Assert.Equal(new IssueId(15), (IssueId) 15);
        }
    }
}