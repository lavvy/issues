﻿using System.Linq;
using Xunit;
using static Issues.Domains.ForTest.IssueFactoryForTest;

namespace Issues.Domains
{
    public sealed class IssueCollectionTest
    {
        [Fact]
        public void OrderBy()
        {
            var issues = NewIssues(3);

            var result = issues.OrderBy();

            Assert.Equal(new[] {3, 2, 1}, result.Select(i => (int) i.Id).ToArray());
        }
    }
}