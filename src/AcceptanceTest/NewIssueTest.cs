﻿using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class NewIssueTest : UseWebDriver
    {
        private readonly Term _term;

        public NewIssueTest()
        {
            _term = new Term();
        }

        [Fact]
        public void ShowNewIssuePage()
        {
            var issuePage = Navigator
                .GoToIssues()
                .ClickNewIssue();
            
            Assert.Equal("New Issue", issuePage.PageTitle);
        }

        [Fact]
        public void NewIssue()
        {
            var issuePage = Navigator
                .GoToIssues()
                .ClickNewIssue();

            var createdPage = issuePage
                .Submit(new {Title = "Title1", Description = "Description1"});
            
            Assert.Equal("Title1", createdPage.Title);
            Assert.Equal("Description1", createdPage.Description);
            Assert.Equal("less than a minute ago", createdPage.TimeAgo);
            Assert.InRange(createdPage.TimeAgo.Timestamp, _term.Low, _term.High);
        }

        [Fact]
        public void ShowCreatedIssue()
        {
            var issuePage = Navigator
                .GoToIssues()
                .ClickNewIssue();

            var createdPage = issuePage
                .Submit(new {Title = "Title1", Description = "Description1"});

            Refresh();
            
            Assert.Equal("Title1", createdPage.Title);
            Assert.Equal("Description1", createdPage.Description);
            Assert.Equal("less than a minute ago", createdPage.TimeAgo);
            Assert.InRange(createdPage.TimeAgo.Timestamp, _term.Low, _term.High);
        }

        [Fact]
        public void RepeatCreation()
        {
            Submit("Title1", "Description1");
            Submit("Title2", "Description2");

            var last = Submit("Title3", "Description3");

            Refresh();
            
            Assert.Equal("Title3", last.Title);
            Assert.Equal("Description3", last.Description);
        }

        private CreationIssuePage GoToCreationPage()
        {
            return Navigator
                .GoToIssues()
                .ClickNewIssue();
            
        }

        private IssuePage Submit(string title, string description)
        {
            return GoToCreationPage()
                .Submit(new {Title = title, Description = description});
        }
    }
}