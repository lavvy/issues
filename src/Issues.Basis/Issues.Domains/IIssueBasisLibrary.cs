﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public interface IIssueBasisLibrary
    {
        Task<IssueBasis> FindAsync(IssueId id);
        Task<IssueBasisCollection> AllAsync();
    }
}