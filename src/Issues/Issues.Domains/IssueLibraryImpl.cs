﻿using System.Linq;
using System.Threading.Tasks;

namespace Issues.Domains
{
    public sealed class IssueLibraryImpl : IIssueLibrary
    {
        private readonly IAuthoredIssueBasisLibrary _basis;
        private readonly IIssueContentRepository _contents;

        public IssueLibraryImpl(IAuthoredIssueBasisLibrary basis, IIssueContentRepository contents)
        {
            _basis = basis;
            _contents = contents;
        }

        public async Task<Issue> FindAsync(IssueId id)
        {
            var basis = await _basis.FindAsync(id);
            return await FindAsync(basis);
        }

        public async Task<IssueCollection> AllAsync()
        {
            var basises = await _basis.AllAsync();

            return basises
                .Select(async basis => await FindAsync(basis))
                .Select(task => task.Result)
                .ExcludeBrokens()
                .ToCollection()
                .OrderBy();
        }

        private async Task<Issue> FindAsync(AuthoredIssueBasis basis)
        {
            var content = await _contents.FindAsync(basis.Id);

            return new Issue(basis, content);
        }
    }
}