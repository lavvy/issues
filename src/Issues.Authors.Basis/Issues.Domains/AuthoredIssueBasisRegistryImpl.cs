﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public sealed class AuthoredIssueBasisRegistryImpl : IAuthoredIssueBasisRegistry
    {
        private readonly IIssueBasisRegistry _basises;
        private readonly IIssueBasisAutoAuthorLibrary _authors;

        public AuthoredIssueBasisRegistryImpl(IIssueBasisRegistry basises, IIssueBasisAutoAuthorLibrary authors)
        {
            _basises = basises;
            _authors = authors;
        }

        public async Task<IssueId> CreateAsync()
        {
            var issueId = await _basises.CreateAsync();

            await _authors.BindAsync(issueId);
            
            return issueId;
        }
    }
}