﻿using System.Threading.Tasks;
using Issues.Users;
using Issues.Users.Domains;
using Moq;
using Xunit;

namespace Issues.Domains
{
    public sealed class AutoAuthorLibraryImplTest
    {
        public AutoAuthorLibraryImplTest()
        {
            _mockAuthors = new Mock<IAuthorLibrary>();
            _mockSignInService = new Mock<IUserSignInLibrary>();

            _target = new AutoAuthorLibraryImpl(_mockAuthors.Object, _mockSignInService.Object);
        }

        private readonly AutoAuthorLibraryImpl _target;
        private readonly Mock<IAuthorLibrary> _mockAuthors;
        private readonly Mock<IUserSignInLibrary> _mockSignInService;
        private readonly ResourceId _testResourceId = (ResourceId) 10;
        private readonly ResourceType _resourceType = (ResourceType) "hoge";
        private static readonly UserName TestUser = (UserName) "testuser";
        private static readonly UserId TestUserId = (UserId) 15;
        private readonly User _testUser = User.Authenticated(TestUserId, TestUser);

        [Fact]
        public async Task BindAsync()
        {
            _mockSignInService.Setup(x => x.GetCurrentUserAsync())
                .Returns(Task.FromResult(_testUser));
            _mockAuthors.Setup(x => x.BindAsync(It.Is<ResourceId>(p => p.Equals(_testResourceId)),
                    It.Is<ResourceType>(p => p.Equals(_resourceType)),
                    It.Is<User>(p => p.Equals(_testUser))))
                .Returns(Task.CompletedTask);

            await _target.BindAsync(_testResourceId, _resourceType);

            _mockSignInService.VerifyAll();
            _mockAuthors.VerifyAll();
        }

        [Fact]
        public async Task FindByResourceIdAsync()
        {
            _mockAuthors.Setup(x => x.FindByResourceIdAsync(It.Is<ResourceId>(p => p.Equals(_testResourceId)),
                    It.Is<ResourceType>(p => p.Equals(_resourceType))))
                .Returns(Task.FromResult(_testUser));

            var result = await _target.FindByResourceIdAsync(_testResourceId, _resourceType);

            Assert.Equal(_testUser.UserName, result.UserName);
        }
    }
}