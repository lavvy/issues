﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Identity
{
    internal static class UserValidatorEnumerableExtensions
    {
        public static Task<IdentityResult> ValidateAsync<T>(this IEnumerable<IUserValidator<T>> validators,
            UserManager<T> userManager, T user) where T : class
        {
            return ValidateUtility
                .ValidateInternalAsync(validators, async v => await v.ValidateAsync(userManager, user));
        }
    }
}