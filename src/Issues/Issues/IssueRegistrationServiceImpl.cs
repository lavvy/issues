﻿using System.Threading.Tasks;
using Issues.Domains;

namespace Issues
{
    public sealed class IssueRegistrationServiceImpl : IIssueRegistrationService
    {
        private readonly IIssueRegistry _issues;
        private readonly IAutoAuthorLibrary _authors;

        public IssueRegistrationServiceImpl(IIssueRegistry issues, IAutoAuthorLibrary authors = null)
        {
            _issues = issues;
            _authors = authors;
        }

        public async Task<IssueId> SubmitAsync(IssueContentViewModel content)
        {
            var issueId = await _issues.CreateAsync((IssueContent) content);

            if (_authors == null) return issueId;

            var resourceId = (ResourceId) (int) issueId;
            await _authors.BindAsync(resourceId, (ResourceType) "issue");

            return issueId;
        }

        public Task SaveAsync(IssueId id, IssueContentViewModel content)
        {
            return _issues.SaveAsync(id, (IssueContent) content);
        }
    }
}