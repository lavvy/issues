﻿using System;

namespace AcceptanceTest
{
    public sealed class Term
    {
        private readonly DateTime _now = DateTime.Now;
        public DateTime Low => _now.AddSeconds(-1);
        public DateTime High => _now.AddSeconds(10);
    }
}