﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public sealed class IssueBasisLibraryImpl : IIssueBasisLibrary
    {
        private readonly IIssueBasisRepository _basises;

        public IssueBasisLibraryImpl(IIssueBasisRepository basises)
        {
            _basises = basises;
        }

        public async Task<IssueBasis> FindAsync(IssueId id)
        {
            return await _basises.FindAsync(id);
        }

        public async Task<IssueBasisCollection> AllAsync()
        {
            return await _basises.AllAsync();
        }
    }
}