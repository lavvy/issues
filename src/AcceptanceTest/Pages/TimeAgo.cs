﻿using System;
using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class TimeAgo : WebStatic
    {
        public TimeAgo(ISearchContext context, string cssSelectorToFind = ".timeago") : base(context, cssSelectorToFind)
        {
        }

        public DateTime Timestamp => DateTime.Parse(Target.GetAttribute("title"));
    }
}