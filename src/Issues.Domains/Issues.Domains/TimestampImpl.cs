﻿using System;

namespace Issues.Domains
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class TimestampImpl : ITimestamp
    {
        public OccurrenceDateTime Now()
        {
            return (OccurrenceDateTime) DateTime.Now;
        }
    }
}