﻿using Issues.Users.Domains;
using Xunit;

namespace Issues.Users
{
    public sealed class UserViewModelTest
    {
        private readonly UserName _testUser = (UserName) "taro";
        private readonly UserId _testUserId = (UserId) 15;

        [Fact]
        public void CastFromAnonymouse()
        {
            var result = (UserViewModel) User.Anonymouse;

            Assert.False(result.IsAuthenticated);
            Assert.Equal(UserName.Anonymouse, result.UserName);
            Assert.Equal(UserId.Empty, result.Id);
        }

        [Fact]
        public void CastFromNullUser()
        {
            var result = (UserViewModel) (User) null;

            Assert.Null(result);
        }

        [Fact]
        public void CastFromUser()
        {
            var result = (UserViewModel) User.Authenticated(_testUserId, _testUser);

            Assert.True(result.IsAuthenticated);
            Assert.Equal(_testUser, result.UserName);
            Assert.Equal(_testUserId, result.Id);
        }

        [Fact]
        public void ConvertToString()
        {
            var target = new UserViewModel(_testUserId, _testUser);
            var result = $"{target}";

            Assert.Equal($"@{(string) _testUser}", result);
        }
    }
}