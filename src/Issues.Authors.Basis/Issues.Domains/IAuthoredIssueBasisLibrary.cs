﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public interface IAuthoredIssueBasisLibrary
    {
        Task<AuthoredIssueBasis> FindAsync(IssueId issueId);
        Task<AuthoredIssueBasisCollection> AllAsync();
    }
}