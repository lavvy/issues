﻿using System.Threading.Tasks;

namespace Issues.Users.Domains
{
    public interface IUserSignInLibrary
    {
        Task<ErrorMessageCollection> SignInAsync(UserName user, Password password);
        Task<User> GetCurrentUserAsync();
        Task SignOutAsync();
    }
}