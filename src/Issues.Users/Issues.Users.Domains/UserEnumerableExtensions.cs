﻿using System.Collections.Generic;
using System.Linq;

namespace Issues.Users.Domains
{
    public static class UserEnumerableExtensions
    {
        public static IEnumerable<UserName> UserNames(this IEnumerable<User> users)
        {
            return users.Select(u => u.UserName);
        }
    }
}