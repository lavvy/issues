﻿using System.Threading.Tasks;

namespace Issues.Users.Domains
{
    public sealed class UserSignInLibraryImpl : IUserSignInLibrary
    {
        private readonly IUserRepository _users;

        public UserSignInLibraryImpl(IUserRepository users)
        {
            _users = users;
        }

        private static ErrorMessageCollection CannotSignInError { get; } = new ErrorMessageCollection(new[]
        {
            (ErrorMessage) "ユーザー名またはパスワードが異なります。"
        });

        public async Task<ErrorMessageCollection> SignInAsync(UserName user, Password password)
        {
            var result = await _users.SignInAsync(user, password);

            return result ? ErrorMessageCollection.NoError : CannotSignInError;
        }

        public async Task<User> GetCurrentUserAsync()
        {
            return await _users.GetCurrentUser();
        }

        public async Task SignOutAsync()
        {
            await _users.SignOutAsync();
        }
    }
}