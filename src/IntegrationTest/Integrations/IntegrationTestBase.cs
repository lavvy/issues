﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Issues;
using Issues.Domains;
using Issues.Users.Infrastructures;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Integrations
{
    public abstract class IntegrationTestBase : IDisposable
    {
        private readonly DateTime _firstDateTime;
        private readonly Mock<ITimestamp> _mockTimestamp;
        private readonly IServiceProvider _services;

        protected IntegrationTestBase()
        {
            _mockTimestamp = new Mock<ITimestamp>();
            _firstDateTime = DateTime.Parse("2017/10/11 12:09:00");
            var current = _firstDateTime;
            _mockTimestamp.Setup(x => x.Now())
                .Returns(() =>
                {
                    current = current.AddSeconds(1);
                    return (OccurrenceDateTime) current;
                });

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.SetupAllProperties();

            var serviceCollection = new ServiceCollection();

            ConfigureServices(serviceCollection);

            IServiceProvider services = serviceCollection.BuildServiceProvider();
            _services = services;

            mockHttpContext.Object.RequestServices = services;
            mockHttpContext.Object.User = new ClaimsPrincipal();
            GetService<SignInManager<ApplicationUser>>()
                .Context = mockHttpContext.Object;
            GetService<IHttpContextAccessor>()
                .HttpContext = mockHttpContext.Object;
        }

        public void Dispose()
        {
            var disposable = _services as IDisposable;
            disposable?.Dispose();
        }

        protected OccurrenceDateTime Offset(int count)
        {
            return (OccurrenceDateTime) _firstDateTime.AddSeconds(count);
        }

        private void ConfigureServices(ServiceCollection services)
        {
            services.AddIssues();

            services.AddInfrastructures(options => options.UseInMemoryDatabase(Guid.NewGuid().ToString()));
            services.AddTransient(provider => _mockTimestamp.Object);
            services.AddTransient<IAuthenticationService>(provider => new AuthenticationServiceStub());
        }

        protected T GetService<T>()
        {
            return _services.GetService<T>();
        }

        private sealed class AuthenticationServiceStub : IAuthenticationService
        {
            public Task<AuthenticateResult> AuthenticateAsync(HttpContext context, string scheme)
            {
                throw new NotImplementedException();
            }

            public Task ChallengeAsync(HttpContext context, string scheme, AuthenticationProperties properties)
            {
                throw new NotImplementedException();
            }

            public Task ForbidAsync(HttpContext context, string scheme, AuthenticationProperties properties)
            {
                throw new NotImplementedException();
            }

            public Task SignInAsync(HttpContext context, string scheme, ClaimsPrincipal principal,
                AuthenticationProperties properties)
            {
                context.User = principal;

                return Task.CompletedTask;
            }

            public Task SignOutAsync(HttpContext context, string scheme, AuthenticationProperties properties)
            {
                context.User = new ClaimsPrincipal();

                return Task.CompletedTask;
            }
        }
    }
}