﻿using System.Collections.Generic;
using System.Linq;

namespace Issues.Domains
{
    public static class IssueEnumerableExtensions
    {
        public static IssueCollection ToCollection(this IEnumerable<Issue> issues)
        {
            return new IssueCollection(issues);
        }

        public static IEnumerable<Issue> ExcludeBrokens(this IEnumerable<Issue> issues)
        {
            return issues.Where(issue => !issue.Content.IsNotFound);
        }
    }
}