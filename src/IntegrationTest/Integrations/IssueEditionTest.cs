﻿using System.Threading.Tasks;
using Issues;
using Xunit;

namespace Integrations
{
    public sealed class IssueEditionTest : IntegrationTestBase
    {
        public IssueEditionTest()
        {
            _library = GetService<IIssueLibraryService>();
            _registry = GetService<IIssueRegistrationService>();
        }

        private readonly IIssueLibraryService _library;
        private readonly IIssueRegistrationService _registry;

        [Fact]
        public async Task SaveAsync()
        {
            var id = await _registry.SubmitAsync(new IssueContentViewModel
            {
                Title = "hello",
                Description = "my name is"
            });

            await _registry.SaveAsync(id, new IssueContentViewModel
            {
                Title = "modified title",
                Description = "modified description"
            });

            var result = await _library.FindAsync(id);

            Assert.Equal(id, result.Id);
            Assert.Equal("modified title", result.Content.Title);
            Assert.Equal("modified description", result.Content.Description);
        }

        [Fact]
        public async Task SaveAsyncNoChangeRegistrationAt()
        {
            var id = await _registry.SubmitAsync(new IssueContentViewModel
            {
                Title = "hello",
                Description = "my name is"
            });

            var created = await _library.FindAsync(id);

            await _registry.SaveAsync(id, new IssueContentViewModel
            {
                Title = "modified title",
                Description = "modified description"
            });

            var result = await _library.FindAsync(id);

            Assert.Equal(created.RegistrationAt, result.RegistrationAt);
        }
    }
}