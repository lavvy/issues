﻿namespace Issues
{
    public static class ValueObjectUtil
    {
        public static string Normalize(string value)
        {
            return value?
                .Trim()
                .Replace("\r\n", "\n")
                .Replace("\r", "\n");
        }
    }
}