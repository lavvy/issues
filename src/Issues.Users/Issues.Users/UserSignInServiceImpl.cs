﻿using System.Threading.Tasks;
using Issues.Users.Domains;

namespace Issues.Users
{
    public sealed class UserSignInServiceImpl : IUserSignInService
    {
        private readonly IUserSignInLibrary _users;

        public UserSignInServiceImpl(IUserRepository users)
        {
            _users = new UserSignInLibraryImpl(users);
        }

        public async Task<ErrorMessageCollection> SignInAsync(UserName user, Password password)
        {
            return await _users.SignInAsync(user, password);
        }

        public async Task<UserViewModel> GetCurrentUserAsync()
        {
            var user = await _users.GetCurrentUserAsync();

            return (UserViewModel) user;
        }

        public async Task SignOutAsync()
        {
            await _users.SignOutAsync();
        }
    }
}