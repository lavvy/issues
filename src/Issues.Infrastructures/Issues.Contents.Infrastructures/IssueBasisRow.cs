﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Issues.Domains;
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Issues.Contents.Infrastructures
{
    [Table("Issues")]
    public class IssueBasisRow
    {
        public int Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime RegistrationAt { get; set; }

        public IssueBasis ToDomain()
        {
            return new IssueBasis((IssueId) Id, (OccurrenceDateTime) RegistrationAt);
        }
    }
}