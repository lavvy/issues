﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Issues.Domains
{
    public sealed class IssueCollection : IEnumerable<Issue>
    {
        private readonly Issue[] _issues;

        public IssueCollection(IEnumerable<Issue> issues)
        {
            _issues = issues.ToArray();
        }

        public IEnumerator<Issue> GetEnumerator()
        {
            return _issues.Cast<Issue>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IssueCollection OrderBy()
        {
            return _issues.OrderBy(i => i).ToCollection();
        }
    }
}