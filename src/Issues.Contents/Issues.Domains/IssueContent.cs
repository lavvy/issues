﻿namespace Issues.Domains
{
    public struct IssueContent
    {
        public IssueTitle Title { get; }
        public IssueDescription Description { get; }
        public bool IsNotFound { get; }

        public IssueContent(IssueTitle title, IssueDescription description) : this(title, description, false)
        {
            Title = title;
            Description = description;
        }

        private IssueContent(IssueTitle title, IssueDescription description, bool isNotFound)
        {
            Title = title;
            Description = description;
            IsNotFound = isNotFound;
        }

        public static IssueContent NotFound = new IssueContent((IssueTitle) "Not Found", (IssueDescription) "", true);

        public override string ToString()
        {
            return $"{nameof(Title)}: {Title}, {nameof(Description)}: {Description}";
        }
    }
}