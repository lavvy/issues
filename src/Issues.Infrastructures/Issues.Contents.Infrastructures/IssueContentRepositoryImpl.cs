﻿using System.Threading.Tasks;
using Issues.Domains;
using Issues.Infrastructures;

namespace Issues.Contents.Infrastructures
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class IssueContentRepositoryImpl : IIssueContentRepository
    {
        private readonly ApplicationDbContext _db;

        public IssueContentRepositoryImpl(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<IssueContent> FindAsync(IssueId id)
        {
            var row = await _db.IssueContents.FindFromAsync(id);
            return IssueContentRow.ToDomain(row);
        }

        public async Task CreateAsync(IssueId id, IssueContent content)
        {
            var row = new IssueContentRow(id);
            await _db.IssueContents.AddAsync(row);

            await ModifyAsync(row, content);
        }

        public async Task SaveAsync(IssueId id, IssueContent content)
        {
            var row = await _db.IssueContents.FindFromAsync(id);

            await ModifyAsync(row, content);
        }

        private async Task ModifyAsync(IssueContentRow row, IssueContent content)
        {
            row.MergeFrom(content);

            await _db.SaveChangesAsync();
        }
    }
}