﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class UserSignInPage : PageBase
    {
        public UserSignInPage(IWebDriver driver) : base(driver)
        {
        }

        private WebControl UserName => FindControl(".signin-input .username");
        private WebControl Password => FindControl(".signin-input .password");

        public ErrorMessageCollection Errors => ErrorMessageCollection.From(Find(".errors"));

        public void SignIn(dynamic login)
        {
            UserName.NewInput((string) login.UserName);
            Password.NewInput((string) login.Password);

            Find(".signin-input .signin").Click();
        }

        public UserRegistrationPage SwitchUserRegistration()
        {
            Find(".switch .registration").Click();

            return new UserRegistrationPage(Driver);
        }
    }
}