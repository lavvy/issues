﻿using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages
{
    public class AboutModel : PageModel
    {
        public string Message { get; private set; }

        // ReSharper disable once UnusedMember.Global
        public void OnGet()
        {
            Message = "Your application description page.";
        }
    }
}
