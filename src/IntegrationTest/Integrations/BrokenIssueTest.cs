﻿using System.Linq;
using System.Threading.Tasks;
using Issues;
using Issues.Contents.Infrastructures;
using Issues.Infrastructures;
using Xunit;

namespace Integrations
{
    public class BrokenIssueTest : IntegrationTestBase
    {
        public BrokenIssueTest()
        {
            _library = GetService<IIssueLibraryService>();
            _registry = GetService<IIssueRegistrationService>();
            _db = GetService<ApplicationDbContext>();
        }

        private readonly IIssueLibraryService _library;
        private readonly ApplicationDbContext _db;
        private readonly IIssueRegistrationService _registry;
    
        [Fact]
        public async Task FindAsync()
        {
            var created = await _db.Issues.AddAsync(new IssueBasisRow());
            await _db.SaveChangesAsync();

            var result = await _library.FindAsync((IssueId) created.Entity.Id);

            Assert.Equal(created.Entity.Id, (int) result.Id);
            Assert.Equal("Not Found", result.Content.Title);
            Assert.Empty(result.Content.Description);
        }

        [Fact]
        public async Task AllAsync()
        {
            var id1 = await _registry.SubmitAsync(new IssueContentViewModel());
            var id2 = await _registry.SubmitAsync(new IssueContentViewModel());
            // Register broken issue
            await _db.Issues.AddAsync(new IssueBasisRow());

            await _db.SaveChangesAsync();

            var result = await _library.AllAsync();
            
            Assert.Equal(2, result.Length);
            Assert.True(result.Any(issue => issue.Id == id1));
            Assert.True(result.Any(issue => issue.Id == id2));
        }
    }
}