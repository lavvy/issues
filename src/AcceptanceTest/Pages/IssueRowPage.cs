﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class IssueRowPage : PageBase
    {
        private readonly IWebElement _row;

        private IssueRowPage(IWebDriver driver, IWebElement row) : base(driver)
        {
            _row = row;
        }

        public WebStatic Title => new WebStatic(_row, ".title");
        public TimeAgo TimeAgo => new TimeAgo(_row);
        public WebStatic Author => new WebStatic(_row, ".author");

        public static IEnumerable<IssueRowPage> Rows(IWebDriver driver, ISearchContext context)
        {
            return context.Finds(".issue")
                .Select(issue => new IssueRowPage(driver, issue));
        }

        public IssuePage ClickLink()
        {
            _row.Find(".link").Click();

            return new IssuePage(Driver);
        }

        public UserProfilePage ClickAuthor()
        {
            Author.Click();
            
            return new UserProfilePage(Driver);
        }
    }
}