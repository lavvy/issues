﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class ErrorMessage
    {
        private readonly string _value;

        private ErrorMessage(string value)
        {
            _value = value;
        }

        public static ErrorMessage From(IWebElement context)
        {
            return context.Text;
        }

        public override string ToString()
        {
            return $"{_value}";
        }

        public static implicit operator string(ErrorMessage message)
        {
            return $"{message}";
        }

        public static implicit operator ErrorMessage(string message)
        {
            return new ErrorMessage(message);
        }
    }
}