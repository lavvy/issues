﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Identity
{
    internal static class ValidateUtility
    {
        internal static Task<IdentityResult> ValidateInternalAsync<TValidator>(IEnumerable<TValidator> validators, Func<TValidator, Task<IdentityResult>> selector) where TValidator : class
        {
            var errors = validators.Select(selector)
                .Select(t => t.Result)
                .SelectMany(r => r.Errors)
                .ToArray();

            var result = errors.Length > 0 ? IdentityResult.Failed(errors) : IdentityResult.Success;

            return Task.FromResult(result);
        }
    }
}