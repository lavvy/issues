﻿using System.Threading.Tasks;

namespace Issues.Domains
{
    public interface IIssueBasisRepository
    {
        Task<IssueId> CreateAsync(OccurrenceDateTime registrationAt);
        Task<IssueBasis> FindAsync(IssueId id);
        Task<IssueBasisCollection> AllAsync();
    }
}