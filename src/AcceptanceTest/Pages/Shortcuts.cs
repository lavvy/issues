﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class Shortcuts : PageBase
    {
        public Shortcuts(IWebDriver driver) : base(driver)
        {
        }

        public CreationIssuePage NewIssue()
        {
            Driver.GoToPage("/Issues/Create");
            return new CreationIssuePage(Driver);
        }

        public IssueListPage Issues()
        {
            Driver.GoToPage("/Issues");

            return new IssueListPage(Driver);
        }

        public UserRegistrationPage UserRegistration()
        {
            Driver.GoToPage("/Users/Register");
            
            return new UserRegistrationPage(Driver);
        }

        public UserSignInPage UserSignIn()
        {
            Driver.GoToPage("/SignIn");
            
            return new UserSignInPage(Driver);
        }

        public IssuePage Issue(string issueId)
        {
            var id = issueId.Replace("#", string.Empty);
            Driver.GoToPage($"/Issues/Show/{id}");

            return new IssuePage(Driver);
        }

        public UserProfilePage MyProfile()
        {
            Driver.GoToPage("/Users/Profile");
            
            return new UserProfilePage(Driver);
        }

        public UserProfilePage UserProfile(string userName)
        {
            Driver.GoToPage($"/Users/Show/{userName}");
            
            return new UserProfilePage(Driver);
        }
    }
}