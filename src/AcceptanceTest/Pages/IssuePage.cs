﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class IssuePage : PageBase
    {
        public IssuePage(IWebDriver driver) : base(driver)
        {
        }

        public WebControl Title => FindControl(".issue .title");
        public WebControl Description => FindControl(".issue .description");
        public WebStatic Id => FindControl(".issue .id");
        public TimeAgo TimeAgo => new TimeAgo(Driver, ".issue .timeago");
        public WebStatic Author => FindControl(".issue .author");
        public string Url => Driver.Url;

        public EditionIssuePage ClickEdit()
        {
            Find(".issue .edit").Click();

            return new EditionIssuePage(Driver);
        }

        public UserProfilePage ClickAuthor()
        {
            Author.Click();

            return new UserProfilePage(Driver);
        }
    }
}