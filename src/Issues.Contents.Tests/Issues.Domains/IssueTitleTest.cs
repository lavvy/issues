﻿using Xunit;

namespace Issues.Domains
{
    public sealed class IssueTitleTest
    {
        [Fact]
        public void CastToString()
        {
            var result = (string) new IssueTitle("hello");
            
            Assert.Equal("hello", result);
        }

        [Fact]
        public void CastFromString()
        {
            Assert.Equal(new IssueTitle("hello"), (IssueTitle) "hello");
        }

        [Fact]
        public void ConvertToString()
        {
            var result = (string) new IssueTitle("hello");
            
            Assert.Equal("hello", result);
        }

        [Fact]
        public void Triming()
        {
            var result = new IssueTitle("  hello  ");
            
            Assert.Equal("hello", (string) result);
        }

        [Fact]
        public void TrimingSpecialCharactors()
        {
            var result = new IssueTitle("  hello \r\n　\t ");
            
            Assert.Equal("hello", (string) result);
        }
    }
}