﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Issues.WebApp
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        private static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseApplicationInsights()
                .Build();
    }
}
