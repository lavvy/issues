﻿using Microsoft.AspNetCore.Identity;

namespace Issues.Users.Infrastructures
{
    public sealed class ApplicationUser : IdentityUser<int>
    {
    }
}