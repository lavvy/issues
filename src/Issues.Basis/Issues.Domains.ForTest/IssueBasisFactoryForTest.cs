﻿using System;
using System.Linq;
using static Issues.Domains.ForTest.IssueIdFactoryForTest;

// ReSharper disable FunctionRecursiveOnAllPaths
// ReSharper disable MemberCanBePrivate.Global

namespace Issues.Domains.ForTest
{
    public static class IssueBasisFactoryForTest
    {
        public static IssueBasis NewBasis(int id, string registrationAt = null)
        {
            return NewBasis(NewId(id), registrationAt);
        }
        
        public static IssueBasis NewBasis(IssueId id, string registrationAt = null)
        {
            var r = registrationAt != null
                ? DateTime.Parse(registrationAt)
                : new DateTime(2015, 1, 1).AddDays((int) id - 1);

            return new IssueBasis(id, (OccurrenceDateTime) r);
        }

        public static IssueBasisCollection NewBasises(int count)
        {
            return Enumerable.Range(1, count)
                .Select(id => NewBasis(id))
                .ToCollection();
        }
    }
}