﻿namespace Issues.Users.Domains
{
    internal sealed class AnonymouseUser : User
    {
        private AnonymouseUser() : base(UserId.Empty, UserName.Anonymouse)
        {
        }

        public override UserType Type { get; } = UserType.Anonymouse;

        public static AnonymouseUser Instance { get; } = new AnonymouseUser();
    }
}