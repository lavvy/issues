﻿using System.Collections.Generic;
using System.Linq;
using Issues;
using Issues.Users;

namespace Integrations
{
    public static class Extensions
    {
        public static IEnumerable<UserViewModel> Authors(this IEnumerable<IssueViewModel> issues)
        {
            return issues.Select(i => i.Author);
        }

        public static IEnumerable<UserName> UserNames(this IEnumerable<UserViewModel> users)
        {
            return users.Select(u => u.UserName);
        }
    }
}