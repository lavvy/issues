﻿using System.Threading.Tasks;
using Issues.Users.Domains;

namespace Issues.Domains
{
    public sealed class AutoAuthorLibraryImpl : IAutoAuthorLibrary
    {
        private readonly IAuthorLibrary _authors;
        private readonly IUserSignInLibrary _signIn;

        public AutoAuthorLibraryImpl(IAuthorLibrary authors, IUserSignInLibrary signIn)
        {
            _authors = authors;
            _signIn = signIn;
        }

        public async Task<User> FindByResourceIdAsync(ResourceId resourceId, ResourceType type)
        {
            return await _authors.FindByResourceIdAsync(resourceId, type);
        }

        public async Task BindAsync(ResourceId resourceId, ResourceType type)
        {
            var currentUser = await _signIn.GetCurrentUserAsync();

            await _authors.BindAsync(resourceId, type, currentUser);
        }
    }
}