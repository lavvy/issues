﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Issues.Authors.Infrastructures
{
    [Table("Authors")]
    public sealed class AuthorRow
    {
        public int ResourceId { get; set; }
        public string Type { get; set; }
        public int AuthorId { get; set; }
    }
}