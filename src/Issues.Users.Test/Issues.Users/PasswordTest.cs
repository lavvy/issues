﻿using Xunit;

namespace Issues.Users
{
    public sealed class PasswordTest
    {
        [Fact]
        public void CastFromString()
        {
            var result = (Password) "hogehoge";

            Assert.Equal(new Password("hogehoge"), result);
        }

        [Fact]
        public void CastToString()
        {
            var result = (string) new Password("hogehoge");

            Assert.Equal("hogehoge", result);
        }

        [Fact]
        public void ConvertToString()
        {
            var result = new Password("hogehoge").ToString();

            Assert.Equal("********", result);
        }
        
        [Fact]
        public void ConvertToStringIncludeNewLine()
        {
            var result = new Password("hoge\nhoge").ToString();

            Assert.Equal("*********", result);
        }

    }
}