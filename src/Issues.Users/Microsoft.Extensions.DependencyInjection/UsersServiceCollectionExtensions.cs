﻿using Issues.Users;
using Issues.Users.Domains;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class UsersServiceCollectionExtensions
    {
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static IServiceCollection AddUsers(this IServiceCollection services)
        {
            services.AddTransient<IUserRegistrationService, UserRegistrationServiceImpl>();
            services.AddTransient<IUserSignInService, UserSignInServiceImpl>();
            services.AddTransient<IUserSignInLibrary, UserSignInLibraryImpl>();
            services.AddTransient<IUserLibrary, UserLibraryImpl>();
            services.AddTransient<IUserLibraryService, UserLibraryServiceImpl>();
            
            return services;
        }
    }
}