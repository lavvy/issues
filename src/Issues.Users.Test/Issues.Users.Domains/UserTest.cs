﻿using Xunit;

namespace Issues.Users.Domains
{
    public sealed class UserTest
    {
        private readonly UserName _testUser = (UserName) "taro";
        private readonly UserId _testUserId = (UserId) 15;

        [Fact]
        public void Anonymouse()
        {
            var result = User.Anonymouse;

            Assert.Equal(UserType.Anonymouse, result.Type);
            Assert.False(result.IsAuthenticated);
            Assert.True(result.IsAnonymouse);
            Assert.Equal(UserName.Anonymouse, result.UserName);
            Assert.Equal(UserId.Empty, result.Id);
        }

        [Fact]
        public void Authenticated()
        {
            var result = User.Authenticated(_testUserId, _testUser);

            Assert.Equal(UserType.Authenticated, result.Type);
            Assert.True(result.IsAuthenticated);
            Assert.False(result.IsAnonymouse);
            Assert.Equal(_testUser, result.UserName);
            Assert.Equal(_testUserId, result.Id);
        }

        [Fact]
        public void ConvertToString()
        {
            var target = User.Authenticated(_testUserId, _testUser);

            var result = target.ToString();

            Assert.Equal("@taro", result);
        }

        [Fact]
        public void Unknown()
        {
            var result = User.Unknown;

            Assert.Equal(UserType.Unknown, result.Type);
            Assert.False(result.IsAuthenticated);
            Assert.False(result.IsAnonymouse);
            Assert.Equal(UserName.Unknown, result.UserName);
            Assert.Equal(UserId.Empty, result.Id);
        }
    }
}