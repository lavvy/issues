﻿using Issues.Domains;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IssuesBasisServiceCollectionExtensions
    {
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static IServiceCollection AddIssueBasis(this IServiceCollection services)
        {
            services.AddTransient<IIssueBasisLibrary, IssueBasisLibraryImpl>();
            services.AddTransient<IIssueBasisRegistry, IssueBasisRegistryImpl>();

            return services;
        }
    }
}