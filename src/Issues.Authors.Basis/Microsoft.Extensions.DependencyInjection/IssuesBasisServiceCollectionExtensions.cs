﻿using System;
using Issues.Domains;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IssuesAuthorsBasisServiceCollectionExtensions
    {
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static IServiceCollection AddAuthorBasises(this IServiceCollection services)
        {
            services.AddTransient(CreateAuthoredIssueBasisLibrary);
            services.AddTransient(CreateAuthoredIssueBasisRegistry);

            return services;
        }

        private static IAuthoredIssueBasisRegistry CreateAuthoredIssueBasisRegistry(IServiceProvider services)
        {
            var authors = CreateIssueBasisAutoAuthorLibrary(services);
            var basises = services.GetService<IIssueBasisRegistry>();

            return new AuthoredIssueBasisRegistryImpl(basises, authors);
        }

        private static IAuthoredIssueBasisLibrary CreateAuthoredIssueBasisLibrary(IServiceProvider services)
        {
            var authors = CreateIssueBasisAutoAuthorLibrary(services);
            var basises = services.GetService<IIssueBasisLibrary>();

            return new AuthoredIssueBasisLibraryImpl(basises, authors);
        }

        private static IIssueBasisAutoAuthorLibrary CreateIssueBasisAutoAuthorLibrary(IServiceProvider services)
        {
            var autoAuthorLibrary = services.GetService<IAutoAuthorLibrary>();
            return new IssueBasisAutoAuthorLibraryImpl(autoAuthorLibrary);
        }
    }
}