﻿using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages
{
    public class ContactModel : PageModel
    {
        private readonly TelemetryClient _telemetry;

        public ContactModel(TelemetryClient telemetry)
        {
            _telemetry = telemetry;
        }

        public string Message { get; private set; }

        // ReSharper disable once UnusedMember.Global
        public void OnGet()
        {
            Message = "Your contact page.";
            
            _telemetry.TrackEvent("Send contat message");
        }
    }
}
