﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Issues.Domains;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace Issues.Contents.Infrastructures
{
    [Table("IssueContents")]
    public class IssueContentRow
    {
        // ReSharper disable once UnusedMember.Global
        public IssueContentRow()
        {
        }

        public IssueContentRow(IssueId id)
        {
            IssueId = (int) id;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        // ReSharper disable once AutoPropertyCanBeMadeGetOnly.Global
        public int IssueId { get; set; }

        [MaxLength(128)]
        [Required]
        public string Title { get; set; }

        [MaxLength(5000)]
        public string Description { get; set; }

        public void MergeFrom(IssueContent content)
        {
            Title = (string) content.Title;
            Description = (string) content.Description;
        }

        public static IssueContent ToDomain(IssueContentRow row)
        {
            return row == null
                ? IssueContent.NotFound
                : new IssueContent((IssueTitle) row.Title, (IssueDescription) row.Description);
        }
    }
}