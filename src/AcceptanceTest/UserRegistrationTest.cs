﻿using AcceptanceTest.Pages;
using Xunit;

namespace AcceptanceTest
{
    public sealed class UserRegistrationTest : UseWebDriver
    {
        private readonly UserRegistrationPage _target;

        public UserRegistrationTest()
        {
            _target = Shortcuts.UserRegistration();
        }

        [Fact]
        public void UserRegistrationTitle()
        {
            Assert.Equal("User registration", _target.PageTitle);
        }

        [Fact]
        public void InitialUserRegistration()
        {
            Assert.Empty((string) _target.UserName);
            Assert.Empty((string) _target.Password);
            Assert.Empty((string) _target.ConfirmPassword);
        }

        [Fact]
        public void UserRegistration()
        {
            _target.Register(new
            {
                UserName = "taro@example.com",
                Password = "efcop3g=DAS",
                ConfirmPassword = "efcop3g=DAS"
            });

            Assert.False(_target.AlreadyTransition);
        }

        [Fact]
        public void PasswordDifference()
        {
            _target.Register(new
            {
                UserName = "taro@example.com",
                Password = "efcop3g=DAS",
                ConfirmPassword = "different"
            });

            Assert.Equal("'Confirm password' and 'Password' do not match.", _target.ConfirmPassword.Errors);
        }

        [Fact]
        public void PasswordTooShort()
        {
            _target.Register(new
            {
                UserName = "taro@example.com",
                Password = "aA_2",
                ConfirmPassword = "aA_2"
            });

            Assert.Equal("Passwords must be at least 6 characters.", _target.Password.Errors);
        }

        [Fact]
        public void PasswordTooSimple()
        {
            _target.Register(new
            {
                UserName = "taro@example.com",
                Password = "1234567890",
                ConfirmPassword = "1234567890"
            });

            Assert.Equal("Passwords must have at least one non alphanumeric character.", _target.Password.Errors);
        }
    }
}