﻿using Xunit;

namespace Issues
{
    public sealed class ValueObjectUtilTest
    {
        [Fact]
        public void TrimSpaces()
        {
            var result = ValueObjectUtil.Normalize("  hello,  world  ");
            
            Assert.Equal("hello,  world", result);
        }

        [Fact]
        public void TrimFullWidthSpaces()
        {
            var result = ValueObjectUtil.Normalize(" 　hello,  world　");
            
            Assert.Equal("hello,  world", result);
        }

        [Fact]
        public void TrimTabs()
        {
            var result = ValueObjectUtil.Normalize("\t\thello,  world\t\t");

            Assert.Equal("hello,  world", result);
        }

        [Fact]
        public void TrimCrLfs()
        {
            var result = ValueObjectUtil.Normalize("\r\n\rhello,  world\r\n\r");
            
            Assert.Equal("hello,  world", result);
        }

        [Fact]
        public void TrimCombine()
        {
            var result = ValueObjectUtil.Normalize("\r\n　 \thello,　world .\r\n\t 　");
            
            Assert.Equal("hello,　world .", result);
        }

        [Fact]
        public void ConvertFromCrToLf()
        {
            var result = ValueObjectUtil.Normalize("[\r]");
            
            Assert.Equal("[\n]", result);
        }

        [Fact]
        public void ConvertFromLfToLf()
        {
            var result = ValueObjectUtil.Normalize("[\n]");
            
            Assert.Equal("[\n]", result);
        }

        [Fact]
        public void ConvertCombine()
        {
            var result = ValueObjectUtil.Normalize("\r\n[\r\n\n \r]\r\n");
            
            Assert.Equal("[\n\n \n]", result);
        }
    }
}