﻿using Xunit;

namespace Issues
{
    public sealed class IssueIdTest
    {
        [Fact]
        public void CastFromInt()
        {
            Assert.Equal(new IssueId(15), (IssueId) 15);
        }

        [Fact]
        public void CastToInt()
        {
            var result = (int) new IssueId(15);

            Assert.Equal(15, result);
        }

        [Fact]
        public void ConvertToString()
        {
            Assert.Equal(new IssueId(15).ToString(), "#15");
        }
    }
}