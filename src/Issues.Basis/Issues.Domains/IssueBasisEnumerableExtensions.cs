﻿using System.Collections.Generic;
using System.Linq;

namespace Issues.Domains
{
    public static class IssueBasisEnumerableExtensions
    {
        public static IEnumerable<IssueId> Ids(this IEnumerable<IssueBasis> basises)
        {
            return basises.Select(b => b.Id);
        }
        public static IEnumerable<OccurrenceDateTime> RegistrationAts(this IEnumerable<IssueBasis> basises)
        {
            return basises.Select(b => b.RegistrationAt);
        }

        public static IssueBasisCollection ToCollection(this IEnumerable<IssueBasis> basises)
        {
            return new IssueBasisCollection(basises);
        }
    }
}