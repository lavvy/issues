﻿using System;
using System.Threading.Tasks;
using Issues.Domains;
using Moq;
using Xunit;
using static Issues.Domains.ForTest.IssueFactoryForTest;

namespace Issues
{
    public sealed class IssueLibraryServiceImplTest
    {
        public IssueLibraryServiceImplTest()
        {
            _mock = new Mock<IIssueLibrary>();

            _target = new IssueLibraryServiceImpl(_mock.Object);
        }

        private readonly IssueLibraryServiceImpl _target;
        private readonly Mock<IIssueLibrary> _mock;

        [Fact]
        public async Task AllAsync()
        {
            _mock.Setup(x => x.AllAsync())
                .Returns(async () => await NewIssuesAsync(3));

            var result = await _target.AllAsync();

            Assert.Equal(3, result.Length);
        }

        [Fact]
        public async Task FindAsync()
        {
            _mock.Setup(r => r.FindAsync(It.IsAny<IssueId>()))
                .Returns(async (IssueId id) => await NewIssueAsync((int) id, "2017/10/9", "Hello", "My name is..."));
            var result = await _target.FindAsync((IssueId) 1);

            Assert.Equal(1, (int) result.Id);
            Assert.Equal(DateTime.Parse("2017/10/9"), (DateTime) result.RegistrationAt);
            Assert.Equal("Hello", result.Content.Title);
            Assert.Equal("My name is...", result.Content.Description);
        }
    }
}