﻿using Xunit;

namespace Issues.Users
{
    public sealed class UserNameTest
    {
        [Fact]
        public void CastFromString()
        {
            var result = (UserName) "hello";

            Assert.Equal(new UserName("hello"), result);
        }

        [Fact]
        public void CastToString()
        {
            var result = (string) new UserName("hello");

            Assert.Equal("hello", result);
        }

        [Fact]
        public void ConvertToString()
        {
            var result = new UserName("hello").ToString();

            Assert.Equal("@hello", result);
        }

        [Fact]
        public void ConvertToStringEmptyUser()
        {
            var result = new UserName(string.Empty).ToString();

            Assert.Empty(result);
        }

        [Fact]
        public void Anonymouse()
        {
            Assert.Equal("@anonymouse", $"{UserName.Anonymouse}");
        }
    }
}