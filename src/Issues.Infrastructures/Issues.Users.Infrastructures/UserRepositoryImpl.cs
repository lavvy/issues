﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Issues.Users.Domains;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;

namespace Issues.Users.Infrastructures
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class UserRepositoryImpl : IUserRepository
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserRepositoryImpl(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IHttpContextAccessor httpContextAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<ErrorMessageCollection> CreateAsync(UserName userName, Password password)
        {
            var result = await _userManager.CreateAsync(new ApplicationUser
            {
                UserName = (string) userName
            }, (string) password);

            var errors = result.Errors.Select(e => (ErrorMessage) e.Description);
            return new ErrorMessageCollection(errors);
        }

        public async Task<ErrorMessageCollection> ValidatePasswordAsync(Password password)
        {
            var result = await _userManager.PasswordValidators
                .ValidateAsync(_userManager, null, (string) password);

            return result.ToErrorMessages().ToCollection();
        }

        public async Task<ErrorMessageCollection> ValidateUserNameAsync(UserName userName)
        {
            var result = await _userManager.UserValidators
                .ValidateAsync(_userManager, ApplicationUser(userName));

            return result.ToErrorMessages().ToCollection();
        }

        public async Task<bool> SignInAsync(UserName userName, Password password)
        {
            var result = await _signInManager.PasswordSignInAsync((string) userName, (string) password, false, false);

            return result.Succeeded;
        }

        public async Task<User> FindAsync(UserId id)
        {
            var result = await _userManager.FindByIdAsync($"{id}");

            return User(result);
        }

        public async Task<User> FindByUserNameAsync(UserName userName)
        {
            var result = await _userManager.FindByNameAsync((string) userName);

            return User(result);
        }

        public Task<User> GetCurrentUser()
        {
            var context = _httpContextAccessor.HttpContext;

            return Task.FromResult(User(context));
        }

        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }

        private static User User(HttpContext context)
        {
            var identity = context.User.Identity;
            var isAuthenticated = identity?.IsAuthenticated ?? false;
            return isAuthenticated ? Authenticated(context.User) : Domains.User.Anonymouse;
        }

        private static User User(ApplicationUser result)
        {
            return result != null ? Authenticated(result) : Domains.User.Anonymouse;
        }

        private static User Authenticated(ApplicationUser result)
        {
            return Domains.User.Authenticated((UserId) result.Id, (UserName) result.UserName);
        }

        private static User Authenticated(ClaimsPrincipal principal)
        {
            var idText = principal.FindFirst(ClaimTypes.NameIdentifier)?.Value ?? "";
            var id = int.Parse(idText);

            return Domains.User.Authenticated((UserId) id, (UserName) principal.Identity.Name);
        }

        private static ApplicationUser ApplicationUser(UserName userName)
        {
            return new ApplicationUser {UserName = (string) userName};
        }
    }
}