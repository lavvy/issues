﻿using Xunit;

namespace Issues
{
    public class ResourceIdTest
    {
        [Fact]
        public void CastFromInt()
        {
            var result = (ResourceId) 15;

            Assert.Equal(new ResourceId(15), result);
        }

        [Fact]
        public void CastToInt()
        {
            var result = (int) new ResourceId(15);

            Assert.Equal(15, result);
        }
    }
}