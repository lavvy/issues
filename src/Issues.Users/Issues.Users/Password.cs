﻿using System.Text.RegularExpressions;

namespace Issues.Users
{
    public struct Password
    {
        private readonly string _value;

        public Password(string value)
        {
            _value = value;
        }

        public static explicit operator string(Password password)
        {
            return password._value;
        }

        public override string ToString()
        {
            return Regex.Replace($"{_value}", ".", "*", RegexOptions.Singleline);
        }

        public static explicit operator Password(string password)
        {
            return new Password(password);
        }
    }
}