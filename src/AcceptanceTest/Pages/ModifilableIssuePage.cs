﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public abstract class ModifilableIssuePage : PageBase
    {
        protected ModifilableIssuePage(IWebDriver driver) : base(driver)
        {
        }

        private WebControl TitleElement => FindControl(".issue-edit .title");
        private WebControl DescriptionElement => FindControl(".issue-edit .description");
        public WebControl Title => TitleElement;
        public WebControl Description => DescriptionElement;

        protected IssuePage Post(dynamic issue, IWebElement postElement)
        {
            Title.NewInput((string) issue.Title);
            Description.NewInput((string) issue.Description);

            postElement.Click();

            return new IssuePage(Driver);
        }
    }
}