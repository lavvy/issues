﻿using System.Threading.Tasks;
using Issues.Users.Domains;

namespace Issues.Domains
{
    public interface IIssueBasisAutoAuthorLibrary
    {
        Task<User> FindByIssueIdAsync(IssueId issueId);
        Task BindAsync(IssueId issueId);
    }
}