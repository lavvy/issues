﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Issues.Domains;

namespace Issues
{
    public sealed class IssueViewModelCollection : IEnumerable<IssueViewModel>
    {
        private readonly IssueViewModel[] _viewModels;

        public IssueViewModelCollection(IEnumerable<IssueViewModel> viewModels)
        {
            _viewModels = viewModels?.ToArray();
        }

        public int Length => _viewModels.Length;
        public bool IsEmpty => _viewModels.Length <= 0;

        public IEnumerator<IssueViewModel> GetEnumerator()
        {
            return _viewModels.Cast<IssueViewModel>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static implicit operator IssueViewModelCollection(IssueCollection issues)
        {
            return From(issues);
        }

        private static IssueViewModelCollection From(IEnumerable<Issue> issues)
        {
            var viewModels = issues.Select(x => (IssueViewModel) x);
            return new IssueViewModelCollection(viewModels);
        }
    }
}