﻿using System;
using AcceptanceTest.Pages;
using OpenQA.Selenium;

namespace AcceptanceTest
{
    public abstract class UseWebDriver : IDisposable
    {
        protected UseWebDriver()
        {
            _driver = WebDriverFactory.GetWebDriver();
        }

        private readonly IWebDriver _driver;

        protected Navigator Navigator => new Navigator(_driver);
        protected Shortcuts Shortcuts => new Shortcuts(_driver);

        protected void Refresh()
        {
            _driver.Navigate().Refresh();
        }

        public void Dispose()
        {
            _driver?.Dispose();
        }
    }
}