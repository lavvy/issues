﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Issues.WebApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        // ReSharper disable once UnusedMember.Global
        public void OnGet()
        {
            _logger.LogDebug("START: IndexModel.OnGet()");
        }
    }
}
