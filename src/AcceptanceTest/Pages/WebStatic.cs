﻿using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public class WebStatic
    {
        private readonly By _by;
        protected readonly ISearchContext Context;

        public WebStatic(ISearchContext context, string cssSelectorToFind) : this(context,
            By.CssSelector(cssSelectorToFind))
        {
        }

        private WebStatic(ISearchContext context, By by)
        {
            Context = context;
            _by = by;
        }

        protected IWebElement Target => Context.FindElement(_by);

        public override string ToString()
        {
            return Target.TagName == "input" ? Target.GetAttribute("value") : Target.Text;
        }

        public static implicit operator string(WebStatic control)
        {
            return $"{control}";
        }

        public void Click()
        {
            Target.Click();
        }
    }
}