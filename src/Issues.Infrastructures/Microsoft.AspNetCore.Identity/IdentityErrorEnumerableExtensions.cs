﻿using System.Collections.Generic;
using System.Linq;
using Issues.Users;

namespace Microsoft.AspNetCore.Identity
{
    internal static class IdentityErrorEnumerableExtensions
    {
        public static IEnumerable<ErrorMessage> ToErrorMessages(this IEnumerable<IdentityError> errors)
        {
            return errors.Select(e => (ErrorMessage) e.Description);
        }
    }
}