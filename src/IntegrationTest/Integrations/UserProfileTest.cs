﻿using System.Threading.Tasks;
using Issues.Users;
using Xunit;

namespace Integrations
{
    public sealed class UserProfileTest : UserRegistrationTestBase
    {
        private readonly Password _password = (Password) "Hogehoge1_";
        private readonly IUserLibraryService _target;
        private readonly UserName _testUser = (UserName) "testuser";

        public UserProfileTest()
        {
            RegisteruserAsync().Wait();

            _target = GetService<IUserLibraryService>();
        }

        private async Task RegisteruserAsync()
        {
            await GetService<IUserRegistrationService>()
                .RegisterAsync(_testUser, _password);
        }

        [Fact]
        public async Task UserProfileAsync()
        {
            var result = await _target.FindByUserNameAsync(_testUser);

            Assert.Equal(_testUser, result.UserName);
        }
    }
}