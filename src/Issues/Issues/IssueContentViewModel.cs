﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Issues.Domains;

namespace Issues
{
    public sealed class IssueContentViewModel
    {
        [DisplayName("Title")]
        [Required]
        [StringLength(128)]
        public string Title { get; set; }
        
        [DisplayName("Description")]
        [StringLength(5000)]
        public string Description { get; set; }

        public static explicit operator IssueContent(IssueContentViewModel viewModel)
        {
            return new IssueContent((IssueTitle) viewModel.Title, (IssueDescription) viewModel.Description);
        }

        public static explicit operator IssueContentViewModel(IssueContent content)
        {
            return new IssueContentViewModel
            {
                Title = (string) content.Title,
                Description = (string) content.Description
            };
        }
    }
}