﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace AcceptanceTest.Pages
{
    public sealed class Navigator : PageBase
    {
        public Navigator(IWebDriver driver) : base(driver)
        {
        }

        public WebStatic SignInUser => new WebStatic(Driver, ".navigator .right .signin-user");
        public bool IsSignIn => Exists(".navigator .right .signin-user");
        public bool IsSignInRegisterShown => Exists(".navigator .right .signin.registration");

        public HomePage GoToHome()
        {
            Find(".navigator .left .menu-item.home").Click();
            return new HomePage(Driver);
        }

        public IssueListPage GoToIssues()
        {
            Find(".navigator .left .menu-item.issues").Click();
            return new IssueListPage(Driver);
        }

        public IEnumerable<string> GetLeftMenuItems()
        {
            return Finds(".navigator .left .menu-item")
                .Select(el => el.Text)
                .ToArray();
        }

        public IEnumerable<string> GetRightMenuItems()
        {
            return Finds(".navigator .right .menu-item")
                .Select(el => el.Text)
                .ToArray();
        }

        public UserSignInPage GoToUserRegistrationOrSignIn()
        {
            Find(".navigator .right .menu-item.registration.signin").Click();

            return new UserSignInPage(Driver);
        }

        public UserProfilePage GoToProfile()
        {
            Find(".navigator .right .signin-user").Click();
            Find(".navigator .right .profile").Click();
            
            return new UserProfilePage(Driver);
        }

        public void SignOut()
        {
            Find(".navigator .right .signin-user").Click();
            Find(".navigator .right .signout").Click();
        }
    }
}