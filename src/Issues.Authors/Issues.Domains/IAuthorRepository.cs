﻿using System.Threading.Tasks;
using Issues.Users;

namespace Issues.Domains
{
    public interface IAuthorRepository
    {
        Task<UserId> FindByResourcedAsync(ResourceId resourceId, ResourceType type);
        Task BindAsync(ResourceId resourceId, ResourceType type, UserId userId);
    }
}