﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace AcceptanceTest.Selenium
{
    public sealed class LocalWebDriverFactory : WebDriverFactory
    {
        protected override IWebDriver CreateFirefoxDriver()
        {
            return new FirefoxDriver();
        }

        protected override IWebDriver CreateChromeDriver()
        {
            return new ChromeDriver();
        }
    }
}