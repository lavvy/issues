using System.Diagnostics;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Issues.WebApp.Pages
{
    public class ErrorModel : PageModel
    {
        public string RequestId { get; private set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        // ReSharper disable once UnusedMember.Global
        public void OnGet()
        {
            RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
        }
    }
}
