﻿using System.Threading.Tasks;

namespace Issues
{
    public interface IIssueLibraryService
    {
        Task<IssueViewModel> FindAsync(IssueId id);
        Task<IssueViewModelCollection> AllAsync();
    }
}